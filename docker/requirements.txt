Django>=4.0
psycopg2>=2.8
django-environ>=1.0
django-crispy-forms>=1.0
mysqlclient >=1.0