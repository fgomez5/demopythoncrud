from django.urls import re_path as url
from django.contrib import admin
from django.conf.urls import include
from myapp import views
from django.urls import include, path

urlpatterns = [
    path('', views.index, name='index'),
    path('created', views.create, name='created'),
    path('update/<id>', views.update, name='update'),
    path('delete/<id>', views.delete, name='delete'),
    ]
