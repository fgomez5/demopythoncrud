from __future__ import unicode_literals

from django.db import models

# Create your models here.
class RegistroPersona(models.Model):
    Nombre = models.CharField(max_length=200)
    Apellido = models.CharField(max_length=200)
    Telefono = models.CharField(max_length=200)
    Correo = models.CharField(max_length=200)


