from django.shortcuts import render, redirect
from myapp.models import RegistroPersona
from myapp.forms import NameForm
from django.shortcuts import redirect

# list.
def index(request):
    names_from_db = RegistroPersona.objects.all()

    form = NameForm()

    context_dict = {'names_from_context': names_from_db, 'form': form}

    if request.method == 'POST':
        form = NameForm(request.POST)

        if form.is_valid():
            form.save(commit=True)
            return render(request, 'index.html', context_dict)
        else:
            print(form.errors)

    return render(request, 'index.html', context_dict)

# Create your views here.
def create(request):

    form = NameForm()
    context_dict = { 'form': form}

    if request.method == 'POST':
        form = NameForm(request.POST)

        if form.is_valid():
            form.save(commit=True)
            return render(request, 'index.html', context_dict)
        else:
            print(form.errors)

    return render(request, 'create.html', context_dict)


# Create your views here.
def update(request, id):

    names_from_db = RegistroPersona.objects.get(id=id)
    form = NameForm(instance=names_from_db)
    context_dict = {'form': form, 'id': id}

    if request.method == 'POST':
        form = NameForm(request.POST, instance=names_from_db)

        if form.is_valid():
            form.save(commit=True)
            return redirect('/')
        else:
            print(form.errors)

    return render(request, 'update.html', context_dict)


# Create your views here.
def delete(request, id):
    RegistroPersona.objects.get(id=id).delete()
    return redirect('/')
