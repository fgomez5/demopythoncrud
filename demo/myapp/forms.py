from django import forms
from myapp.models import RegistroPersona

class NameForm(forms.ModelForm):
    Nombre = forms.CharField(max_length=100, help_text = "Coloque su Nombre")
    Apellido = forms.CharField(max_length=100, help_text="Coloque su Apellido")
    Telefono = forms.CharField(max_length=100, help_text="Coloque su Telefono")
    Correo = forms.CharField(max_length=100, help_text="Coloque su Correo")

    class Meta:
        model = RegistroPersona
        fields = ('Nombre','Apellido','Telefono','Correo')